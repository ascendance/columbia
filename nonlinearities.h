
double simpledatars(double x, double y, double alpha, double beta, double shift);

double sqnl(double s, double gamma);

double sqnlderiv(double s, double gamma);

double expsolution(double x, double y, double alpha, double beta);

double sinxsolution(double x, double gammal, double gammar);

double sinxrs(double x, double gammal, double gammar);

double sinxfluxx(double x, double gammal, double gammar);

double expX(double x, double alpha);

double expY(double y, double beta);

double expXs(double x,double alpha);

double expYs(double y, double beta);

double expfluxx(double x, double y, double alpha, double beta, double gammal, double gammag, double gammar, double shift, double gap);

double expfluxy(double x, double y, double alpha, double beta, double gammal, double gammag, double gammar, double shift, double gap);

double exprs(double x, double y, double alpha, double beta, double gammal, double gammag, double gammar, double shift, double gap);

double IronNL(double);

double ArmNL(double);

double AirNL(double);

double SimpleRs(double, double, double, double, double, double);

double SimpleDeriv(double, double, double, double, double, double, double);

double SimpleNL(double, double, double, double, double, double, double);

double test12Rs(double, double, double, double, double, double);

double test12Deriv(double, double, double, double, double, double, double);

double test12NL(double, double, double, double, double, double, double);

double MagnetRs(double, double, double, double, double, double);

double MagnetDeriv(double, double, double, double, double, double, double);

double MagnetNL(double, double, double, double, double, double, double);

double EngineRs(double, double, double, double, double);

double EngineDeriv(double, double, double, double, double);

double EngineNL(double, double, double, double, double);


